<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html"/>

<xsl:template match="/travaux">
<html lang="{langue}"  xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:foaf="http://xmlns.com/foaf/0.1/">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta property="dc:type" content="e-Portfolio"/>

        <meta property="dc:language" content="{langue}"/>

        <meta property="dc:creator" content="Mau G"/>

        <title property="dc:title"><xsl:value-of select="titre"/></title>

        <link rel="icon" href="../../common/img/favicon.jpg" type="image/jpg" sizes="16x16"/>
        
        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@700&amp;family=Prompt:wght@300&amp;display=swap" rel="stylesheet"/> 
        <!-- style -->
        <link rel="stylesheet" type="text/css" media="screen" href="../../common/style/common.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"/>
        <!-- icons -->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    </head>

    <body>
        <!-- entete -->
        <header class="navbar sticky-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="../../index/{langue}/index.xml" property="foaf:homepage">maug.me</a>

                <nav class="justify-content-end" id="navbarNavAltMarkup">
                    <div class="nav">
                        <a class="nav-link active" aria-current="page" href="../../travaux/{langue}/travaux.xml" property="foaf:publications"><xsl:value-of select="//navigation/choix[@id=1]"/></a>
                        <a class="nav-link" href="../../parcours/{langue}/parcours.xml" property="foaf:page"><xsl:value-of select="//navigation/choix[@id=2]"/></a>
                        <a class="nav-link" href="../../a_propos/{langue}/a_propos.xml" property="foaf:page"><xsl:value-of select="//navigation/choix[@id=3]"/></a>
                        <div class="dropdown nav-link">
                            <a class="btn dropdown-toggle p-0" href="#" role="button" id="language-btn" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-globe"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="language-btn">
                                <li>
                                    <a class="dropdown-item" href="../fr/travaux.xml"><img src="../../common/flags/fr.png" alt="fr"/> <xsl:value-of select="//navigation/langues/langue[@id='fr']"/></a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../vi/travaux.xml"><img src="../../common/flags/vn.png" alt="vi"/> <xsl:value-of select="//navigation/langues/langue[@id='vi']"/></a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../th/travaux.xml"><img src="../../common/flags/th.png" alt="th"/> <xsl:value-of select="//navigation/langues/langue[@id='th']"/></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <!-- corps -->
        <main class="container pb-5 my-5">
            <div class="row row-cols-3 row-cols-md-3 row-cols-sm-1 g-0" about="#Mau G">
                <xsl:apply-templates select="corps/projet"/>
            </div>
        </main>

        <!-- bas de page -->
        <footer class="container-fluid mt-5 fixed-bottom pt-2">
            <div class="row">
                <ul class="list-unstyled list-inline text-center col mb-0" about="#Mau G">
                    <li id="pinterest" class="list-inline-item">
                        <a href="https://www.pinterest.fr/mau_me/" property="foaf:page"><i class="fab fa-pinterest"></i></a>
                    </li>
                    <li id="discord" class="list-inline-item">
                        <a href="https://discord.gg/xuuUkAd6xv" property="foaf:account"><i class="fab fa-discord"></i></a>
                    </li>
                    <li id="instagram" class="list-inline-item">
                        <a href="https://www.instagram.com/nuna.m.e/" property="foaf:page"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="row text-center">
                <xsl:apply-templates select="//basdepage"/>
            </div>
        </footer>

        <!-- scripts -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </body>
</html>
</xsl:template>

<xsl:template match="corps/projet">
    <div class="col" about="{description/attribute::lien}">
        <a href="{description/attribute::lien}" target="_blank" property="foaf:pastProject">
            <div class="card bg-white text-white text-center h-100 rounded-0 border-0">
                <img src="{description/attribute::min}" class="card-img rounded-0" alt="{./attribute::type}" property="foaf:img"/>
                <div class="card-img-overlay">
                    <h5 class="card-title text"><xsl:value-of select="intitule"/></h5>
                    <p class="card-text text"><xsl:apply-templates select="description"/></p>
                    <small class="card-text text">
                        <xsl:value-of select="@date"/>
                    </small>
                    <xsl:if test="description/attribute::auteur">
                        <span lang="en">ref : <small property="dc:creator"><xsl:value-of select="description/attribute::auteur"/></small></span>
                    </xsl:if>
                </div>
            </div>
        </a>
    </div>
</xsl:template>

<xsl:template match="collaborateur">
    <strong  property="foaf:knows" about="#Mau G">
        <span typeof="foaf:Person">
            <span property="foaf:name">
                <a href="http://omgcvtp.cluster029.hosting.ovh.net" ref="foaf:homepage">
                    <xsl:value-of select="."/>
                </a>
            </span>
        </span>
    </strong>
</xsl:template>

<!-- bas de page -->
<xsl:template match="//basdepage">
    <p>
        ©<xsl:apply-templates />
    </p>
</xsl:template>

<xsl:template match="datecreation">
    <small property="dc:date"><xsl:value-of select="."/></small>
</xsl:template>

<xsl:template match="auteure">
    <span property="dc:creator foaf:name"><xsl:value-of select="."/></span>
</xsl:template>
</xsl:stylesheet>
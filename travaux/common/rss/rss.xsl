<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:media="http://search.yahoo.com/mrss/">

<xsl:output method="html"/>
<xsl:template match="/rss/channel">
        <html lang="fr" dir="ltr">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <!-- insérer le titre de la chaine ici -->
                <title>
                    <xsl:value-of select="title"/>
                </title>
                <link rel="preconnect" href="https://fonts.gstatic.com"></link>
                <link href="https://fonts.googleapis.com/css2?family=Roboto&amp;family=Roboto+Slab:wght@800&amp;display=swap" rel="stylesheet"></link>
                <link rel="stylesheet" type="text/css" href="rss.css" media="screen"></link>
            </head>
            <body>
                <div class="container">
                    <xsl:apply-templates select="item"/>
                </div>
            </body>
        </html>
    </xsl:template>

    <!-- template d'un item -->
    <xsl:template match="item">
        <article class="article">
            <h2 class="titre">
                <xsl:value-of select="title"/>
            </h2>
            <!-- si image -->
            <xsl:if test="./media:content">
                <a href="{link}" target="_blank">
                    <img class="img" src="{media:content/@url}" alt="pas d'image"/>
                </a>
            </xsl:if>   

            <!-- description -->
            <p class="description">
                <xsl:value-of select="description"/>
            </p>

            <!-- depeche -->
            <span class="date-publication">
                publié le <xsl:value-of select="pubDate"/>
            </span>
        </article>
    </xsl:template>
</xsl:stylesheet>
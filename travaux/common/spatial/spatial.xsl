<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:output method="html"/>
    <xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" dir="ltr">
			<head>
				<meta charset="utf-8"/>
				<title>Programmes spatiaux</title>
				<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather|Open+Sans"/>
				<link rel="stylesheet" type="text/css" media="screen" href="spatial.css" />
			</head>
			<body>
				<main xmlns:dc="http://purl.org/dc/elements/1.1/">
					<div class="intro">
						<h1 property="dc:title">Programmes spatiaux américains et russes</h1>
						<h2>Par <span class="creator" property="dc:creator">Aminata Mbodji</span> et <span class="creator" property="dc:creator">Maurane Glaude</span></h2>
						<h4 property="dc:type">Astronomie</h4>
					</div>
					
					<div class="container">
						<hr/>
						<xsl:apply-templates/>
					</div>
				</main>
			</body>
		</html>
    </xsl:template>

	<xsl:template match="programmes">
		<div class="program" xmlns:dc="http://purl.org/dc/elements/1.1/">
         	<xsl:apply-templates select="./nation"/>
		</div>
	</xsl:template>

    <xsl:template match="nation">
		<h1>
			<xsl:value-of select="./@nom"/> : leurs programme spatiaux
		</h1>
		<xsl:apply-templates select="programme"/>
	</xsl:template>
	
	<xsl:template match="programme">
		<h2><xsl:value-of select="./@nom"/></h2>
		<ul>
			<li>Début : <strong><span property="dc:date"><xsl:value-of select="./@debut"/></span></strong></li>
			<li>Fin : <strong><span property="dc:date"><xsl:value-of select="./@fin"/></span></strong></li>
			<li>Objectif : <strong><span property="dc:subject"><xsl:value-of select="./objectif"/></span></strong></li>
			<li>Nombre de mission : <xsl:value-of select="./@missions"/></li>
			<li>Liste des missions :
				<ul>
					<xsl:apply-templates select="mission"/>
				</ul>
			</li>
		</ul>
	</xsl:template>
	
	<xsl:template match="mission">
		<li>
			<h3>
				<xsl:value-of select="./@nom"/>
			</h3>
			<ul xmlns:dc="http://purl.org/dc/elements/1.1/">
				<li>
					<xsl:choose>
						<xsl:when test="./but">
							But : <span property="dc:description"><xsl:value-of select="./but"/></span>
						</xsl:when>
						<xsl:otherwise>
						Pas de but<br/>
						</xsl:otherwise>
					</xsl:choose>
				</li>

				<li>
					<xsl:choose>
						<xsl:when test="./evenement">
							Évènement : <span property="dc:description"><xsl:value-of select="./evenement"/></span>
						</xsl:when>
						<xsl:otherwise>
							Pas d'évènement
						</xsl:otherwise>
					</xsl:choose>
				</li>
			</ul>
		</li>
	</xsl:template>

</xsl:stylesheet>
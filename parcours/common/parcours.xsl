<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html"/>

<xsl:template match="/parcours">
<html lang="{langue}" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:foaf="http://xmlns.com/foaf/0.1/">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta property="dc:type" content="e-Portfolio"/>

        <meta property="dc:language" content="{langue}"/>

        <title property="dc:title"><xsl:value-of select="titre"/></title>

        <link rel="icon" href="../../common/img/favicon.jpg" type="image/jpg" sizes="16x16"/>

        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@700&amp;family=Prompt:wght@300&amp;display=swap" rel="stylesheet"/> 
        <!-- style -->
        <link rel="stylesheet" type="text/css" media="screen" href="../../common/style/common.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"/>
        <!-- icons -->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    </head>
    <body>

        <!-- entete -->
        <header class="navbar sticky-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="../../index/{langue}/index.xml" property="foaf:homepage">maug.me</a>

                <nav class="justify-content-end" id="navbarNavAltMarkup">
                    <div class="nav">
                        <a class="nav-link active" aria-current="page" href="../../travaux/{langue}/travaux.xml" property="foaf:publications"><xsl:value-of select="//navigation/choix[@id=1]"/></a>
                        <a class="nav-link" href="../../parcours/{langue}/parcours.xml "><xsl:value-of select="//navigation/choix[@id=2]"/></a>
                        <a class="nav-link" href="../../a_propos/{langue}/a_propos.xml"><xsl:value-of select="//navigation/choix[@id=3]"/></a>
                        <div class="dropdown nav-link">
                            <a class="btn dropdown-toggle p-0" href="#" role="button" id="language-btn" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-globe"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="language-btn">
                                <li>
                                    <a class="dropdown-item" href="../fr/parcours.xml"><img src="../../common/flags/fr.png" alt="fr"/>&#160;<xsl:value-of select="//navigation/langues/langue[@id='fr']"/></a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../vi/parcours.xml"><img src="../../common/flags/vn.png" alt="vi"/>&#160;<xsl:value-of select="//navigation/langues/langue[@id='vi']"/></a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../th/parcours.xml"><img src="../../common/flags/th.png" alt="th"/>&#160;<xsl:value-of select="//navigation/langues/langue[@id='th']"/></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <!-- corps -->
        <div id="main-container" class="container-fluid">
            <div class="row">
                <aside id="details" class="col-3">
                    <xsl:apply-templates select="corps/details"/>
                </aside>

                
                <main class="col">
                    <h1 property="foaf:primaryTopic dc:subject" about="#Mau G"><xsl:value-of select="corps/principal/attribute::titre"/></h1>
                    <section id="experiences-container" class="row">
                        <div class="sous-titre d-flex align-items-center">
                            <h3 ><xsl:value-of select="corps/principal/experiences/attribute::intitule"/></h3>
                            <hr class="line w-75"/>
                        </div>
                        <xsl:apply-templates select="//experience"/>
                    </section>

                    <section id="formations-container" class="row" about="#Mau G">
                        <div class="sous-titre d-flex align-items-center">
                            <h3><xsl:value-of select="corps/principal/formations/attribute::intitule"/></h3>
                            <hr class="line w-75"/>
                        </div>
                        <xsl:apply-templates select="//formation"/>
                    </section>

                    <section id="competences-container" class="row" about="#Mau G">
                        <div class="sous-titre d-flex align-items-center">
                            <h3 ><xsl:value-of select="corps/principal/competences/attribute::intitule"/></h3>
                            <hr class="line w-75"/>
                        </div>
                        <xsl:apply-templates select="//competence"/>
                    </section>
                </main>
            </div>
        </div>

        <!-- bas de page -->
        <footer class="container-fluid mt-5 fixed-bottom pt-2">
            <div class="row">
                <ul class="list-unstyled list-inline text-center col mb-0" about="#Mau G">
                    <li id="pinterest" class="list-inline-item">
                        <a href="https://www.pinterest.fr/mau_me/" target="_blank" property="foaf:page"><i class="fab fa-pinterest"></i></a>
                    </li>
                    <li id="discord" class="list-inline-item">
                        <a href="https://discord.gg/xuuUkAd6xv" target="_blank" property="foaf:account"><i class="fab fa-discord"></i></a>
                    </li>
                    <li id="instagram" class="list-inline-item">
                        <a href="https://www.instagram.com/nuna.m.e/" target="_blank" property="foaf:page"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="row text-center">
                <xsl:apply-templates select="//basdepage"/>
            </div>
        </footer>

        <!-- scripts -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </body>

</html>

</xsl:template>

<!-- gabarit détails -->
<xsl:template match="corps/details">
    <section id="profil-container" class="mb-4 mt-3" about="#Mau G">
        <div class="d-flex justify-content-center">
            <img src="../../common/img/madam.png" class="img-thumbnail" alt="photo" style="border-radius: 50%" property="foaf:img"/>
        </div>
        <p>
            <ul class="list-unstyled">
                <li class="mb-4 mt-2">
                    <h4 class="text-center" property="foaf:name">
                        <xsl:value-of select="profil/prenom"/>&#160;<xsl:value-of select="profil/nomdefamille"/>
                    </h4>
                    <h5 class="text-center" property=""><xsl:value-of select="profil/profession"/></h5>
                </li>
                <li>
                    <i class="fas fa-birthday-cake"></i>&#160;<span property="foaf:age"><xsl:value-of select="profil/age"/></span>&#160;<xsl:value-of select="//@unite"/>
                </li>
                <li>
                    <i class="fas fa-map-marker-alt"></i>&#160;<span property="foaf:based_near"><xsl:value-of select="profil/localisation/ville"/>, <xsl:value-of select="profil/localisation/pays"/></span>
                </li>
                <li>
                    <i class="fas fa-cubes"></i>&#160;<span property="foaf:myersBriggs">INFP</span>
                </li>
            </ul>
        </p>
    </section>

    <section id="contact-container" class="mb-4">
        <div class="sous-titre d-flex">
            <h5>
                <xsl:value-of select="contact/attribute::nom"/>
            </h5>
            <hr class="line w-75"/>
        </div>
        <div>
            <xsl:apply-templates select="contact"/>
        </div>
    </section>

    <section id="interest-container" class="mb-4">
        <div class="sous-titre d-flex">
            <h5>
                <xsl:value-of select="interets/attribute::nom"/>
            </h5>
            <hr class="line w-75"/>
        </div>
        <div>
            <xsl:apply-templates select="interets/interet"/>
        </div>
    </section>

    <section id="langues-container">
        <div class="sous-titre d-flex">
            <h5>
                <xsl:value-of select="langues/attribute::nom"/>
            </h5>
            <hr class="line w-75"/>
        </div>
        <div>
            <xsl:apply-templates select="langues/langue"/>
        </div>
    </section>
</xsl:template>

<!-- gabarit contact -->
<xsl:template match="contact">
    <address about="#Mau G">
        <span id="email"><i class="fas fa-at small"></i>&#160;<a class="small" href="mailto:{email}"><xsl:value-of select="email"/></a></span><br/>
        <span>
            <i class="fas fa-phone"></i>&#160;
            <a class="small" href="tel:+33622334455" property="foaf:phone" about="#Mau G"><xsl:value-of select="tel"/></a>
        </span><br/>
        <span>
            <i class="fas fa-blog"></i>&#160;<a class="small" href="{portfolio_artistique/attribute::lien}" property="foaf:publications"><xsl:value-of select="portfolio_artistique"/></a>
        </span>
    </address>
</xsl:template>

<!-- gabarit interet -->
<xsl:template match="interets/interet">
    <dl about="#Mau G">
        <dt class="interet"><span property="foaf:topic_interest"><xsl:value-of select="nom"/></span>
            <xsl:if test="@apprentissage">
                (<xsl:value-of select="@apprentissage"/>)
            </xsl:if> 
        </dt>
        <dd>
            <xsl:apply-templates select="productions/production" />
        </dd>
    </dl>
</xsl:template>

<!-- gabarit productions -->
<xsl:template match="productions">
    <dd>
        <xsl:apply-templates select="production"/>
    </dd>
</xsl:template>

<!-- gabarit production -->
<xsl:template match="productions/production">
    <p class="mb-2">
        <xsl:choose>
            <xsl:when test="@lien">
                <a href="{@lien}" property="foaf:publications"><xsl:value-of select="."/></a>
            </xsl:when>
            <xsl:otherwise>
                <span><xsl:value-of select="."/></span>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if test="@duree">
            (<xsl:if test="@association"><xsl:value-of select="@association"/>, </xsl:if>
                <xsl:value-of select="@duree"/> <xsl:value-of select="@unite"/>)
        </xsl:if>
    </p>
</xsl:template>

<!-- gabarit langue -->
<xsl:template match="langue">
    <dl>
        <dt><xsl:value-of select="nom"/></dt>
        <dd><xsl:value-of select="niveau"/></dd>
    </dl>
</xsl:template>


<!-- gabarit experience -->
<xsl:template match="//experience">
<dl>
    <dt>
        <span class="tile px-2">
            <xsl:value-of select="@debut"/> - <xsl:if test="@fin"><xsl:value-of select="@fin"/></xsl:if>
        </span>
    </dt>
    <dd>
        <dl>
            <dt><xsl:value-of select="poste"/>, <xsl:value-of select="@entreprise"/> (<xsl:value-of select="@ville"/>, <xsl:value-of select="@pays"/>)</dt>
            <dd>
                <xsl:for-each select="description/tache">
                    <p><xsl:value-of select="."/></p>
                </xsl:for-each>
            </dd>
        </dl>
    </dd>
</dl>
</xsl:template>

<!-- gabarit formation -->
<xsl:template match="//formation">
<dl>
    <dt>
        <span class="tile px-2">
            <xsl:value-of select="@debut"/> - <xsl:if test="@fin"><xsl:value-of select="@fin"/></xsl:if>
        </span>
    </dt>
    <dd>
        <dl>
            <dt>
                <xsl:value-of select="diplome"/>
                <xsl:if test="diplome/attribute::abreviation">
                    (<xsl:value-of select="diplome/attribute::abreviation"/>)
                </xsl:if>
                |
                <a href="{ufr/attribute::lien}" property="foaf:schoolHomepage">
                    <xsl:choose>
                        <xsl:when test="ufr/attribute::abreviation">
                            <abbr title="{ufr}" class="initialism"><xsl:value-of select="ufr/attribute::abreviation"/></abbr>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="ufr"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </a> - 
                <a href="{ecole/attribute::lien}" property="foaf:schoolHomepage"> 
                    <abbr title="{ecole}" class="initialism"><xsl:value-of select="ecole/attribute::abreviation"/></abbr>
                </a>
                (<xsl:value-of select="ville"/>,<xsl:value-of select="pays"/>)
            </dt>
            <dd>
                <xsl:for-each select="description/tache">
                    <p><xsl:value-of select="."/></p>
                </xsl:for-each>
            </dd>
        </dl>
    </dd>
</dl>
</xsl:template>

<!-- gabarit competence -->
<xsl:template match="//competence">
    <dl>
        <dt>
            <xsl:value-of select="nom"/> (<xsl:value-of select="@type"/>)
        </dt>
        <dd>
            <xsl:choose>
                <xsl:when test="langages">
                    <dl>
                        <dt>
                            <xsl:value-of select="framework"/>
                        </dt>
                        <dd>
                            <xsl:variable name="lastlangage" select="langages/langage[last()]"/>
                            <xsl:for-each select="langages/langage">
                                <xsl:choose>
                                    <xsl:when test=". != $lastlangage">
                                        <xsl:value-of select="."/>, 
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="."/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </dd>
                    </dl>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="logiciel"/>
                </xsl:otherwise>
            </xsl:choose>
        </dd>
    </dl>
</xsl:template>



<xsl:template match="//basdepage">
    <p>
        ©<xsl:apply-templates />
    </p>
</xsl:template>

<xsl:template match="datecreation">
    <small property="dc:date"><xsl:value-of select="."/></small>
</xsl:template>

<xsl:template match="auteure">
    <span property="dc:creator"><xsl:value-of select="."/></span>
</xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html"/>

<xsl:template match="/accueil">
<html lang="{langue}" xmlns="http://www.w3.org/1999/xhtml" xmlns:dc="http://purl.org/dc/elements/1.1/" >
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta property="dc:type" content="e-Portfolio"/>

        <meta property="dc:language" content="{langue}"/>
        <meta property="dc:creator" content="Mau G"/>

        <title property="dc:title"><xsl:value-of select="titre"/></title>

        <link rel="icon" href="../../common/img/favicon.jpg" type="image/jpg" sizes="16x16"/>

        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@700&amp;family=Prompt:wght@300&amp;display=swap" rel="stylesheet"/> 
        <!-- style -->
        <link rel="stylesheet" type="text/css" media="screen" href="../../common/style/common.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"/>
        <!-- icons -->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    </head>
    <body>
        <!-- entete -->
        <header class="navbar sticky-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="../{langue}/index.xml" property="foaf:homepage">maug.me</a>

                <nav class="justify-content-end" id="navbarNavAltMarkup">
                    <div class="nav">
                        <a class="nav-link active" aria-current="page" href="../../travaux/{langue}/travaux.xml" property="foaf:publications"><xsl:value-of select="//navigation/choix[@id=1]"/></a>
                        <a class="nav-link" href="../../parcours/{langue}/parcours.xml"><xsl:value-of select="//navigation/choix[@id=2]"/></a>
                        <a class="nav-link" href="../../a_propos/{langue}/a_propos.xml"><xsl:value-of select="//navigation/choix[@id=3]"/></a>
                        <div class="dropdown nav-link">
                            <a class="btn dropdown-toggle p-0" href="#" role="button" id="language-btn" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-globe"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="language-btn">
                                <li>
                                    <a class="dropdown-item" href="../../index.html"><img src="../../common/flags/fr.png" alt="fr"/>&#160;<xsl:value-of select="//navigation/langues/langue[@id='fr']"/></a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../vi/index.xml"><img src="../../common/flags/vn.png" alt="vi"/>&#160;<xsl:value-of select="//navigation/langues/langue[@id='vi']"/></a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../th/index.xml"><img src="../../common/flags/th.png" alt="th"/>&#160;<xsl:value-of select="//navigation/langues/langue[@id='th']"/></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <!-- corps -->
        <main id="index" class="container d-flex flex-column d-flex justify-content-around">
            <h1 class="mb-5"><xsl:value-of select="corps/titre"/></h1>
            <h2 property="dc:description"><xsl:value-of select="corps/description"/></h2>
        </main>

        <!-- bas de page -->
        <footer class="container-fluid mt-5 fixed-bottom pt-2">
            <div class="row">
                <ul class="list-unstyled list-inline text-center col mb-0" about="#Mau G">
                    <li id="pinterest" class="list-inline-item">
                        <a href="https://www.pinterest.fr/mau_me/" target="_blank" property="foaf:page"><i class="fab fa-pinterest"></i></a>
                    </li>
                    <li id="discord" class="list-inline-item">
                        <a href="https://discord.gg/xuuUkAd6xv" target="_blank" property="foaf:account"><i class="fab fa-discord"></i></a>
                    </li>
                    <li id="instagram" class="list-inline-item">
                        <a href="https://www.instagram.com/nuna.m.e/" target="_blank" property="foaf:page"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="row text-center">
                <xsl:apply-templates select="//basdepage"/>
            </div>
        </footer>
    </body>
    <!-- scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</html>
    
</xsl:template>

<xsl:template match="//basdepage">
    <p>
        ©<xsl:apply-templates />
    </p>
</xsl:template>

<xsl:template match="datecreation">
    <small property="dc:date"><xsl:value-of select="."/></small>
</xsl:template>

<xsl:template match="auteure">
    <span property="dc:creator"><xsl:value-of select="."/></span>
</xsl:template>

</xsl:stylesheet>